export const gridSize = 10;
export const boxSize = 50;
export const firstSelected = false;
export const loseFlag = false;

export const colorOfSelected = '#4A148C';
export const colorOfGrid = '#EF9A9A';
export const colorOfPotential = '#BA68C8';
