import * as GAME_OPTIONS from './game-options.js';
import * as CONFIG from './object-config.js';
import * as INTERFACE from './interface.js';

let {
	boxSize, firstSelected, loseFlag, colorOfSelected, colorOfGrid, colorOfPotential,
} = GAME_OPTIONS;

const { createBoxParameters } = CONFIG;

let score = 0;

export let prevMove = [];

export const colorBlack = (object, numberOfPotentialBoxes) => {
	const newNumberOfPotentialBoxes = numberOfPotentialBoxes + 1;
	object.set('fill', colorOfPotential);
	return newNumberOfPotentialBoxes;
};

export const resetGame = (currentCanvas) => {
	currentCanvas.getObjects().map((o) => {
		o.set('fill', colorOfGrid);
	});

	loseFlag = false;
	firstSelected = false;
	score = 0;
	INTERFACE.score[0].innerHTML = score;
	prevMove = [];
	currentCanvas.renderAll()
};


export const grid = (x, y, canvas) => {
	const box = new fabric.Rect(createBoxParameters(x, y));
	
	box.on('selected', () => {
		const selectedBox = canvas.getActiveObject();
		const currentX = selectedBox.left;
		const currentY = selectedBox.top;
		let numberOfPotentialBoxes = 0;

		if (
			(selectedBox.get('fill') === colorOfGrid && firstSelected === false)
            || (selectedBox.get('fill') === colorOfPotential && firstSelected === true)
		) {
			canvas.getObjects().map((o) => {
				if (o.get('fill') === colorOfSelected) return;
				if (o.get('fill') === colorOfPotential && o.get('fill') !== colorOfSelected) {
					o.set('fill', colorOfGrid);
				}
				if (
					(o.left === currentX + boxSize * 3 && o.top === currentY)
                    || (o.top === currentY + boxSize * 3 && o.left === currentX)
                    || (o.left === currentX - boxSize * 3 && o.top === currentY)
                    || (o.top === currentY - boxSize * 3 && o.left === currentX)
                    || (o.top === currentY + boxSize * 2 && o.left === currentX + boxSize * 2)
                    || (o.top === currentY - boxSize * 2 && o.left === currentX - boxSize * 2)
                    || (o.top === currentY + boxSize * 2 && o.left === currentX - boxSize * 2)
                    || (o.top === currentY - boxSize * 2 && o.left === currentX + boxSize * 2)
				) {
					numberOfPotentialBoxes = colorBlack(o, numberOfPotentialBoxes);
				}
			});

			selectedBox.set('fill', colorOfSelected);
			firstSelected = true;

			if (!numberOfPotentialBoxes) loseFlag = true;

			if (loseFlag) {
				INTERFACE.score[1].innerHTML = score;
				INTERFACE.loseModal.style.display = 'block';
				
				resetGame(canvas)
			} else {
				score++;
				if (score === 100) {
					INTERFACE.winModal.style.display = 'block';
				}
			}

			INTERFACE.score[0].innerHTML = score;
			prevMove.push(canvas.getObjects())
		}

		// console.log(prevMove)
		canvas.renderAll();
	});
	return box;
};
