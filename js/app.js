import * as GAME_OPTIONS from './game-options.js';
import * as RENDER from './render.js';
import * as INTERFACE from './interface.js';

const canvas = new fabric.Canvas('c');
canvas.hoverCursor = 'pointer';

export const generateGrid = (gridSize, reset, prevCanvas) => {
	let x = 0;
	let y = 0;
	const resetGameState = reset;
	for (y = 0; y < gridSize; y++) {
		for (x = 0; x < gridSize; x++) {
			const posY = GAME_OPTIONS.boxSize * y;
			const posX = GAME_OPTIONS.boxSize * x;
			canvas.add(RENDER.grid(posX, posY, canvas, resetGameState, prevCanvas));
		}
	}
};

INTERFACE.resetY[0].onclick = () => {
	INTERFACE.resetModal.style.display = 'none';
	RENDER.resetGame(canvas)
};

INTERFACE.resetY[1].onclick = () => {
	INTERFACE.winModal.style.display = 'none';
	RENDER.resetGame(canvas)
};

// INTERFACE.undo.onclick = () => {
// 	let lastMove = RENDER.prevMove.length

// 	generateGrid(GAME_OPTIONS.gridSize, true, RENDER.prevMove[lastMove - 1]);
// }

generateGrid(GAME_OPTIONS.gridSize);
