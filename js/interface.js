export const resetButton = document.querySelector('button#resetButton');
export const score = document.getElementsByClassName('score');

export const loseModal = document.querySelector('#loseModal');
export const winModal = document.querySelector('#winModal');
export const resetModal = document.querySelector('#resetModal');

export const resetY = document.getElementsByClassName('resetYes');
export const resetN = document.getElementsByClassName('closeModal');

// export const undo = document.querySelector('#undo');


resetButton.onclick = () => {
	resetModal.style.display = 'block';
};

resetN[0].onclick = () => {
	loseModal.style.display = 'none';
};
resetN[1].onclick = () => {
	resetModal.style.display = 'none';
};
