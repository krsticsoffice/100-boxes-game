import { boxSize, colorOfGrid } from './game-options.js';

export const boxParameters = {
	width: boxSize,
	height: boxSize,
	fill: colorOfGrid,
	lockMovementX: true,
	lockMovementY: true,
	lockRotation: true,
	lockScalingFlip: true,
	lockUniScaling: true,
	hasBorders: false,
	hasControls: false,
	rx: 5,
	ry: 5,
};

export const createBoxParameters = (x, y) => {
	const newBoxParameters = { ...boxParameters, top: y, left: x };
	return newBoxParameters;
};
